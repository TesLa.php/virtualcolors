<?php

/*
 * Este es un programa privado destinado a evaluación para la empresa virtualcolors,
 * se distribuye bajo una Licencia Creative Commons Atribución-NoComercial-SinDerivar 4.0 Internacional.
 * Debería haber recibido una copia de la Licencia junto con este programa.
 * Si no es así, consulte <http://creativecommons.org/licenses/by-nc-nd/4.0/>.
 */

namespace Modelo;

use Nut\Conexion\Sentencia;

/**
 * Descripcion de Politica
 *
 * @author Neder Alfonso Fandiño Andrade <neafan@hotmail.com>
 */
class Politica extends \Nut\Modelo {

    public $nombreEmpresa;
    public $descripcion;

    public function existe() {
        return $this->_buscar();
    }

    public function insertar() {
        $sentencia = new Sentencia([
          'sql' => "INSERT INTO `politica`(`nombreEmpresa`, `descripcion`) VALUES (:id, :ds)",
          'valores' => ['id' => $this->nombreEmpresa, 'ds' => $this->descripcion]
        ]);
        return parent::insertar($sentencia);
    }

    public function buscar() {
        $politica = $this->_buscar();
        return $this->poblar($politica);
    }

    /**
     *
     * @param Politica $objeto
     * @param object $dato
     */
    protected function poblarObjeto($objeto, $dato) {
        $objeto->nombreEmpresa = $dato->nombreEmpresa;
        $objeto->descripcion = $dato->descripcion;
    }

    public function actualizar() {
        $sentencia = new Sentencia([
          'sql' => "UPDATE `politica` SET `descripcion` = :ds WHERE `nombreEmpresa` = :id",
          'valores' => ['id' => $this->nombreEmpresa, 'ds' => $this->descripcion]
        ]);
        return parent::ejecutar($sentencia);
    }

    private function _buscar() {
        $sentencia = new Sentencia([
          'sql' => "SELECT * FROM `politica` WHERE `nombreEmpresa` = :id",
          'valores' => ['id' => $this->nombreEmpresa]
        ]);
        return parent::seleccionar($sentencia);
    }

}
