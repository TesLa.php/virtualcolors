<?php

/*
 * Este es un programa privado destinado a evaluación para la empresa virtualcolors,
 * se distribuye bajo una Licencia Creative Commons Atribución-NoComercial-SinDerivar 4.0 Internacional.
 * Debería haber recibido una copia de la Licencia junto con este programa.
 * Si no es así, consulte <http://creativecommons.org/licenses/by-nc-nd/4.0/>.
 */

namespace Modelo;

use Nut\Conexion\{
    Sentencia
};

/**
 * Descripcion de Rol
 *
 * @author Neder Alfonso Fandiño Andrade <neafan@hotmail.com>
 */
class Rol extends \Nut\Modelo {

    public $nombre;
    public $nombreEmpresa;

    /**
     *
     * @param Rol $objeto
     * @param object $dato
     */
    protected function poblarObjeto($objeto, $dato) {
        $objeto->nombre = $dato->nombre;
        $objeto->nombreEmpresa = $dato->nombreEmpresa;
    }

    public function existe() {
        return $this->buscar();
    }

    public function actualizar() {
        $sentencia = new Sentencia([
          'sql' => "UPDATE `rol` SET `nombreEmpresa` = :ne WHERE `nombre` = :id",
          'valores' => ['ne' => $this->nombreEmpresa, 'id' => $this->nombre]
        ]);
        return parent::ejecutar($sentencia);
    }

    public function insertar() {
        $sentencia = new Sentencia([
          'sql' => "INSERT INTO `rol`(`nombreEmpresa`, `nombre`) VALUES (:ne, :id)",
          'valores' => ['ne' => $this->nombreEmpresa, 'id' => $this->nombre]
        ]);
        return parent::insertar($sentencia);
    }

    public function buscar() {
        $sentencia = new Sentencia([
          'sql' => "SELECT * FROM `rol` WHERE `nombre` = :id",
          'valores' => ['id' => $this->nombre]
        ]);
        $rol = parent::seleccionar($sentencia);
        return parent::poblar($rol);
    }

}
