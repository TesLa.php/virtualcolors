<?php

/*
 * Este es un programa privado destinado a evaluación para la empresa virtualcolors,
 * se distribuye bajo una Licencia Creative Commons Atribución-NoComercial-SinDerivar 4.0 Internacional.
 * Debería haber recibido una copia de la Licencia junto con este programa.
 * Si no es así, consulte <http://creativecommons.org/licenses/by-nc-nd/4.0/>.
 */

namespace Modelo;

use Nut\Conexion\Sentencia;

/**
 * Descripcion de Responsabilidad
 *
 * @author Neder Alfonso Fandiño Andrade <neafan@hotmail.com>
 */
class Responsabilidad extends \Nut\Modelo {

    public $id;
    public $detalle;
    public $nombreEmpresa;
    public $nombreRol;

    protected function poblarObjeto($objeto, $dato) {
        $objeto->id = $dato->id;
        $objeto->detalle = $dato->detalle;
        $objeto->nombreRol = $dato->nombreRol;
        $objeto->nombreEmpresa = $dato->nombreEmpresa;
    }

    public function existe() {
        return $this->_buscar();
    }

    public function actualizar() {
        $sentencia = new Sentencia([
          'sql' => "UPDATE `responsabilidad` SET `detalle` = :dt WHERE `id` = :id AND `nombreRol` = :nr AND `nombreEmpresa` = :ne",
          'valores' => ['ne' => $this->nombreEmpresa, 'id' => $this->id, 'dt' => $this->detalle, 'nr' => $this->nombreRol]
        ]);
        return parent::ejecutar($sentencia);
    }

    public function insertar() {
        $sentencia = new Sentencia([
          'sql' => "INSERT INTO `responsabilidad` (`id`, `detalle`, `nombreRol`, `nombreEmpresa`) VALUES (:id, :dt, :nr, :ne)",
          'valores' => ['ne' => $this->nombreEmpresa, 'id' => $this->id, 'dt' => $this->detalle, 'nr' => $this->nombreRol]
        ]);
        return parent::insertar($sentencia);
    }

    public function buscar() {
        $responsabilidad = $this->_buscar();
        return $this->poblar($responsabilidad);
    }

    protected function _buscar() {
        $sentencia = new Sentencia([
          'sql' => "SELECT * FROM `responsabilidad` WHERE `id` = :id AND nombreRol = :nr AND nombreEmpresa = :ne",
          'valores' => ['id' => $this->id, 'nr' => $this->nombreRol, 'ne' => $this->nombreEmpresa]
        ]);
        return parent::seleccionar($sentencia);
    }

}
