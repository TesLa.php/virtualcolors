<?php

namespace Nut\ReglaURL;

function formatearConUpperCamelCase($cadena) {
    return preg_replace("/[-._]/", "", ucwords($cadena));
}

function patronMVC($segmentos) {
    /* Extraemos segmentos */
    $controlador = formatearConUpperCamelCase(array_shift($segmentos) ?? 'principal');
    $metodo = formatearConUpperCamelCase(array_shift($segmentos) ?? 'inicio');
    $argumentos = $segmentos;

    $Clase = "Controlador\\$controlador";
    call_user_func_array([new $Clase(), $metodo], $argumentos);
}
