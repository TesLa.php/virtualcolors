<?php

/*
 * Este es un programa privado destinado a evaluación para la empresa virtualcolors,
 * se distribuye bajo una Licencia Creative Commons Atribución-NoComercial-SinDerivar 4.0 Internacional.
 * Debería haber recibido una copia de la Licencia junto con este programa.
 * Si no es así, consulte <http://creativecommons.org/licenses/by-nc-nd/4.0/>.
 */

error_reporting(~E_WARNING);

use Nut\{
    Enrutador,
    AutoCargador
};

require './vendor/autoload.php';

$servidor = filter_input(INPUT_SERVER, 'SERVER_NAME');
$directorio = dirname(filter_input(INPUT_SERVER, 'SCRIPT_NAME'));

$dominio = str_replace('//', '/', "$servidor$directorio");

define('URL_BASE', "http://$dominio/");
define('RUTA_RAIZ', __DIR__ . DIRECTORY_SEPARATOR);

AutoCargador::cargarConfiguracion([
  "espacios" => [
    "Controlador" => "Controlador",
    "Modelo" => "Modelo",
    "Libreria" => "Libreria",
    "Nut" => "Libreria/Nut"
  ]
]);


Enrutador::$rutaReglas = "configuraciones/reglasURL";

Enrutador::agregarRegla("[^/]*", function ($coincidencias) {
    /* Eliminamos el criterio de busqueda del arreglo */
    $segmentos = array_filter(array_shift($coincidencias));
    Enrutador::ejecutarFuncion("patronMVC", $segmentos, null);
});
Enrutador::ejecutarRegla(filter_input(INPUT_GET, "url"));
