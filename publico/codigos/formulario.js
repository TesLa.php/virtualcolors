/*
 * Este es un programa privado destinado a evaluación para la empresa virtualcolors,
 * se distribuye bajo una Licencia Creative Commons Atribución-NoComercial-SinDerivar 4.0 Internacional.
 * Debería haber recibido una copia de la Licencia junto con este programa.
 * Si no es así, consulte <http://creativecommons.org/licenses/by-nc-nd/4.0/>.
 */

/* global CKEDITOR, fetch */

require(["configuracion"], function () {
    require(["bootstrap", "jquery", "ckeditor", "toolimg", "sweetalert"], function () {

        CKEDITOR.replace("descripcion-empresa");

        /**
         *  previsualizacion de la firma del responsable
         */
        function leerImagen(campo) {
            if (campo.files && campo.files[0]) {
                var lectura = new FileReader();
                lectura.onload = function (e) {
                    $('#vista-previa-firma').attr('href', e.target.result);
                    $('#vista-previa-firma img').attr('src', e.target.result);
                };
                lectura.readAsDataURL(campo.files[0]);
            }
        }
        $("#firma-empresa").change(function () {
            leerImagen(this);
        });


        $("#formulario-politica").submit(function (e) {
            e.preventDefault();
            var formulario = new FormData(this);
            fetch('politica/guardar', {
                body: formulario,
                method: "post"
            }).then(function (promesa) {
                return promesa.json();
            }).then(function (respuesta) {
                if (respuesta) {
                    swal("¡Perfecto!", `La empresa de "${formulario.get("nombre-empresa")}" se ha guardado exitosamente'.`, "success");
                } else {
                    swal("¡Espera!", "Algo ha salido mal", "error");
                }
            });
        });

        /**
         * Adiciona un uevo rol solo si no existe
         */
        $("#boton-guardar-rol").click(crearRol = function () {
            var $rol = $("#nombre-rol");
            if (!$(`#responsabilidades tr[data-rol="${$rol.val()}"]`).length) {
                $("#responsabilidades").append(`<tr data-rol="${$rol.val()}">
                    <td>
                        ${$rol.val()}
                    </td>
                    <td>
                        <ul class="responsabilidades"></ul>
                    </td>
                </tr>`);
            }
            return $(`[data-rol="${$rol.val()}"]`);
        });

        /**
         * Adiciona un nuevo item de responsabilidad solo si no existe para el rol
         */
        $("#boton-guardar-responsable").click(function () {
            var $rol = $("#nombre-rol");
            var $responsabilidad = $("#nombre-responsabilidad");
            var $filaRol = $(`[data-rol="${$rol.val()}"]`);
            if (!$filaRol.length) {
                $filaRol = crearRol();
            }
            if ($responsabilidad.val().length) {
                var $responsabilidades = $filaRol.find('.responsabilidades');
                var noExiste = true;
                $responsabilidades.find("li").each(function () {
                    if ($(this).text() === $responsabilidad.val()) {
                        noExiste = false;
                    }
                });

                if (noExiste) {
                    $responsabilidades.append(
                            `<li>
                                <div class="responsabilidad">${$responsabilidad.val()}</div>
                                <button type="button" data-accion="eliminar-responsabilidad" class="btn btn-danger">Eliminar</button>
                            </li>`);

                    $responsabilidades.find(`button[data-accion="eliminar-responsabilidad"]`).click(function () {
                        var $filaRol = $(this).parents("tr[data-rol]");
                        $filaRol.remove();
                    });
                }
            }
        });

        $("#formulario-rol").submit(function (e) {
            e.preventDefault();
            var formulario = new FormData();
            var $nombreEmpresa = $("#nombre-empresa");
            formulario.append('nombre-empresa', $nombreEmpresa.val());

            $("[data-rol]").each(function () {
                var $filaRol = $(this);
                $filaRol.find('.responsabilidades li .responsabilidad').each(function () {
                    formulario.append(`roles[${$filaRol.attr("data-rol")}][]`, $(this).text());
                });
            });
            fetch('rol/guardarTodo', {
                body: formulario,
                method: "post"
            }).then(function (promesa) {
                return promesa.json();
            }).then(function (respuesta) {
                console.log(respuesta)
                if (respuesta) {
                    swal("¡Perfecto!", `Todos los roles y responsabilidades de "${$nombreEmpresa.val()}" se ha guardado exitosamente'.`, "success");
                } else {
                    swal("¡Espera!", "Algo ha salido mal", "error");
                }
            });
        });
    });
});