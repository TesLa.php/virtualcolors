/*
 * Este es un programa privado destinado a evaluación para la empresa virtualcolors,
 * se distribuye bajo una Licencia Creative Commons Atribución-NoComercial-SinDerivar 4.0 Internacional.
 * Debería haber recibido una copia de la Licencia junto con este programa.
 * Si no es así, consulte <http://creativecommons.org/licenses/by-nc-nd/4.0/>.
 */

require.config({
    baseUrl: "publico",
    urlArgs: `compilacion=${(new Date()).getTime()}`, //Deshabiitar CACHE
    paths: {
        "jquery": "../publico/librerias/jquery/dist/jquery.min",
        "bootstrap": "../publico/librerias/bootstrap/dist/js/bootstrap.min",
        "ckeditor": "../publico/librerias/ckeditor/ckeditor",
        "toolimg": "../publico/librerias/terceros/tooltip-image-preview",
        "sweetalert": "../publico/librerias/sweetalert/dist/sweetalert.min"
    },
    shim: {
        "bootstrap": {
            deps: ["jquery"]
        },
        "toolimg": {
            deps: ["jquery"]
        }
    }
});
