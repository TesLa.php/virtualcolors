<?php

/*
 * Este es un programa privado destinado a evaluación para la empresa virtualcolors,
 * se distribuye bajo una Licencia Creative Commons Atribución-NoComercial-SinDerivar 4.0 Internacional.
 * Debería haber recibido una copia de la Licencia junto con este programa.
 * Si no es así, consulte <http://creativecommons.org/licenses/by-nc-nd/4.0/>.
 */

namespace Nut;

use Nut\Conexion\{
    Conexion,
    BaseDatos,
    Excepcion
};

/**
 * Descripcion de Modelo
 *
 * @author Neder Alfonso Fandiño Andrade <neafan@hotmail.com en nekoos.com>
 */
abstract class Modelo {

    /**
     *
     * @var \Nut\Conexion\Proveedor
     */
    public $bd;
    public $excepcion;

    public function __construct() {
        $this->bd = new Conexion(new BaseDatos([
          'usuario' => 'root',
          'nombre' => 'virtualcolors'
        ]));
        $this->excepcion = new \Exception();
    }

    /**
     *
     * @param Modelo $dato
     * @param object $objeto
     */
    public function poblar($dato, $objeto = null) {
        $retorno = false;
        if (!is_object($objeto)) {
            $objeto = $this;
        }
        if (boolval($dato)) {
            $this->poblarObjeto($objeto, $dato);
            $retorno = true;
        }
        return $retorno;
    }

    protected abstract function poblarObjeto($objeto, $dato);

    public function __sleep() {
        return array_diff(array_keys(get_object_vars($this)), array('bd'));
    }

    protected function ejecutar($sentencia) {
        $retorno = true;
        try {
            $this->bd->ejecutar($sentencia);
        } catch (Excepcion $excepcion) {
            $this->excepcion = $excepcion;
            $retorno = false;
        }
        return $retorno;
    }

    protected function seleccionar($sentencia) {
        try {
            $retorno = $this->bd->seleccionar($sentencia);
        } catch (Excepcion $excepcion) {
            $this->excepcion = $excepcion;
            $retorno = false;
        }
        return $retorno;
    }

    protected function seleccionarTodo($sentencia) {
        try {
            $retorno = $this->bd->seleccionarTodo($sentencia);
        } catch (Excepcion $excepcion) {
            $this->excepcion = $excepcion;
            $retorno = false;
        }
        return $retorno;
    }

    protected function insertar($sentencia) {
        $retorno = true;
        try {
            $this->bd->insertar($sentencia);
        } catch (Excepcion $excepcion) {
            $this->excepcion = $excepcion;
            $retorno = false;
        }
        return $retorno;
    }

}
