<?php

/*
 * Este es un programa privado destinado a evaluación para la empresa virtualcolors,
 * se distribuye bajo una Licencia Creative Commons Atribución-NoComercial-SinDerivar 4.0 Internacional.
 * Debería haber recibido una copia de la Licencia junto con este programa.
 * Si no es así, consulte <http://creativecommons.org/licenses/by-nc-nd/4.0/>.
 */

namespace Nut;

/**
 * Description de Vista
 *
 * @author Neder Alfonso Fandiño Andrade <neafan@hotmail.com en nekoos.com>
 */
class Vista {

    private static $directorioBase = 'vistas';
    private static $extensionBase = '.phtml';
    private $_datos = [];

    protected function existeArchivo(string $archivo): bool {
        return file_exists($archivo);
    }

    protected function obtenerRutaReal(string $archivo): string {
        if (!$this->existeArchivo($archivo)) {
            $archivo = "{$this::$directorioBase}/$archivo";
            if (!pathinfo($archivo, PATHINFO_EXTENSION)) {
                $archivo .= self::$extensionBase;
            }
        }
        return $archivo;
    }

    /**
     *
     * @param string $archivo
     * @return string
     */
    public function obtenerHtml(string $archivo): string {
        $retorno = '';
        $rutaArchivo = $this->obtenerRutaReal($archivo);
        if ($this->existeArchivo($rutaArchivo)) {
            ob_start();
            include $rutaArchivo;
            $retorno = ob_get_clean();
        }
        /** Fix para convertir url's relativas como absolutas, solo es util para poder autocompletar en el IDE */
        return preg_replace('/(\.\.\/)+/', URL_BASE, $retorno);
    }

    /**
     *
     * @param string $archivo
     * @return \Nut\Vista
     */
    protected function cargarHtml(string $archivo): Vista {
        echo $this->obtenerHtml($archivo);
    }

    /**
     *
     * @param string $atributo
     * @return mixed
     */
    public function &__get(string $atributo) {
        return $this->_datos[$atributo];
    }

    /**
     *
     * @param string $atributo
     * @param mixed $valor
     */
    public function __set(string $atributo, $valor) {
        $this->_datos[$atributo] = $valor;
    }

}
