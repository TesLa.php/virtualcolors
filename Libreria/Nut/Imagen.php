<?php

/*
 * Este es un programa privado destinado a evaluación para la empresa virtualcolors,
 * se distribuye bajo una Licencia Creative Commons Atribución-NoComercial-SinDerivar 4.0 Internacional.
 * Debería haber recibido una copia de la Licencia junto con este programa.
 * Si no es así, consulte <http://creativecommons.org/licenses/by-nc-nd/4.0/>.
 */

namespace Nut;

/**
 * Descripcion de Imagen
 *
 * @author Neder Alfonso Fandiño Andrade <neafan@hotmail.com>
 */
class Imagen {

    protected function validar($imagen) {
        $archivo = $imagen["tmp_name"];
        $tipo = exif_imagetype($archivo);
        return (($tipo == IMAGETYPE_JPEG) || ($tipo == IMAGETYPE_PNG));
    }

    public function existe($origenImagen) {
        return isset($_FILES[$origenImagen]);
    }

    public function guardar(string $origenImagen, string $destinoImagen) {
        $retorno = -1;
        if ($this->existe($origenImagen)) {
            $retorno = 0;
            $imagen = $_FILES[$origenImagen];
            if ($this->validar($imagen)) {
                $retorno = $this->_guardar($imagen, $destinoImagen);
            }
        }
        return $retorno;
    }

    private function _guardar(array $imagen, string $destinoImagen) {
        $retorno = 0;
        if (move_uploaded_file($imagen['tmp_name'], $destinoImagen)) {
            $retorno = 1;
        }
        return $retorno;
    }

}
