DROP SCHEMA IF EXISTS `virtualcolors` ;
CREATE SCHEMA IF NOT EXISTS `virtualcolors` DEFAULT CHARACTER SET utf8 ;
USE `virtualcolors` ;

DROP TABLE IF EXISTS `politica` ;
CREATE TABLE IF NOT EXISTS `politica` (
  `nombreEmpresa` VARCHAR(45) NOT NULL,
  `descripcion` TEXT NOT NULL,
  PRIMARY KEY (`nombreEmpresa`))
ENGINE = InnoDB;

DROP TABLE IF EXISTS `rol` ;
CREATE TABLE IF NOT EXISTS `rol` (
  `nombre` VARCHAR(45) NOT NULL,
  `nombreEmpresa` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`nombre`, `nombreEmpresa`),
  INDEX `fk_rol_politica1_idx` (`nombreEmpresa` ASC),
  CONSTRAINT `fk_rol_politica1`
    FOREIGN KEY (`nombreEmpresa`)
    REFERENCES `politica` (`nombreEmpresa`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

DROP TABLE IF EXISTS `responsabilidad` ;
CREATE TABLE IF NOT EXISTS `responsabilidad` (
  `id` INT UNSIGNED NOT NULL,
  `detalle` VARCHAR(100) NOT NULL,
  `nombreRol` VARCHAR(45) NOT NULL,
  `nombreEmpresa` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`, `nombreRol`, `nombreEmpresa`),
  INDEX `fk_responsabilidad_rol1_idx` (`nombreRol` ASC, `nombreEmpresa` ASC),
  CONSTRAINT `fk_responsabilidad_rol1`
    FOREIGN KEY (`nombreRol` , `nombreEmpresa`)
    REFERENCES `rol` (`nombre` , `nombreEmpresa`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;