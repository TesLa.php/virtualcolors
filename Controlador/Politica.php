<?php

/*
 * Este es un programa privado destinado a evaluación para la empresa virtualcolors,
 * se distribuye bajo una Licencia Creative Commons Atribución-NoComercial-SinDerivar 4.0 Internacional.
 * Debería haber recibido una copia de la Licencia junto con este programa.
 * Si no es así, consulte <http://creativecommons.org/licenses/by-nc-nd/4.0/>.
 */

namespace Controlador;

use Nut\Imagen;

/**
 * Descripcion de Politica
 *
 * @author Neder Alfonso Fandiño Andrade <neafan@hotmail.com>
 */
class Politica {

    public function guardar() {
        $nombreEmpresa = filter_input(INPUT_POST, 'nombre-empresa');
        $descripcionEmpresa = filter_input(INPUT_POST, 'descripcion-empresa');

        $Politica = new \Modelo\Politica();
        $Politica->nombreEmpresa = $nombreEmpresa;
        $Politica->descripcion = $descripcionEmpresa;

        if ($Politica->existe()) {
            $accion = $Politica->actualizar();
        } else {
            $accion = $Politica->insertar();
        }

        $Imagen = new Imagen();
        $accion &= $Imagen->guardar('firma-empresa', "publico/imagenes/firmas/$nombreEmpresa.png");

        echo json_encode(boolval($accion));
    }

}
