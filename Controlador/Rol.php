<?php

/*
 * Este es un programa privado destinado a evaluación para la empresa virtualcolors,
 * se distribuye bajo una Licencia Creative Commons Atribución-NoComercial-SinDerivar 4.0 Internacional.
 * Debería haber recibido una copia de la Licencia junto con este programa.
 * Si no es así, consulte <http://creativecommons.org/licenses/by-nc-nd/4.0/>.
 */

namespace Controlador;

/**
 * Descripcion de Rol
 *
 * @author Neder Alfonso Fandiño Andrade <neafan@hotmail.com>
 */
class Rol {

    public function guardar() {
        $nombreRol = filter_var($_POST['nombre-rol']);
        $nombreEmpresa = filter_var($_POST['nombre-empresa']);

        $Rol = new \Modelo\Rol();
        $Rol->nombre = $nombreRol;
        $Rol->nombreEmpresa = $nombreEmpresa;

        if ($Rol->existe()) {
            $accion = $Rol->actualizar();
        } else {
            $accion = $Rol->insertar();
        }

        return $accion;
    }

    public function guardarTodo() {
        $roles = filter_input(INPUT_POST, 'roles', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $nombreEmpresa = filter_input(INPUT_POST, 'nombre-empresa');

        $accion = true;
        foreach ($roles as $nombreRol => $responsabilidades) {
            $_POST['nombre-rol'] = $nombreRol;
            $accion &= $this->guardar();
            foreach ($responsabilidades as $id => $responsabilidad) {
                $Responsabilidad = new \Modelo\Responsabilidad();
                $Responsabilidad->id = $id + 1;
                $Responsabilidad->detalle = $responsabilidad;
                $Responsabilidad->nombreRol = $nombreRol;
                $Responsabilidad->nombreEmpresa = $nombreEmpresa;
                if ($Responsabilidad->existe()) {
                    $accion &= $Responsabilidad->actualizar();
                } else {
                    $accion &= $Responsabilidad->insertar();
                }
            }
        }
        echo json_encode($accion);
    }

}
