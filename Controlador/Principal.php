<?php

/*
 * Este es un programa privado destinado a evaluación para la empresa virtualcolors,
 * se distribuye bajo una Licencia Creative Commons Atribución-NoComercial-SinDerivar 4.0 Internacional.
 * Debería haber recibido una copia de la Licencia junto con este programa.
 * Si no es así, consulte <http://creativecommons.org/licenses/by-nc-nd/4.0/>.
 */

namespace Controlador;

use Nut\Vista;

/**
 * Descripcion de Principal
 *
 * @author Neder Alfonso Fandiño Andrade <neafan@hotmail.com>
 */
class Principal {

    public function inicio() {
        $vista = new Vista();
        echo $vista->obtenerHtml("principal");
    }

    public function formulario() {
        $vista = new Vista();
        echo $vista->obtenerHtml("formulario");
    }

}
