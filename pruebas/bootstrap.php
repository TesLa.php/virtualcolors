<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author Neder Alfonso Fandiño Andrade <neafan@hotmail.com>
 */
use Nut\{
    AutoCargador
};

error_reporting(E_ALL & ~E_WARNING);
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');

require_once '../vendor/autoload.php';

define('URL_BASE', "http://localhost/virtualcolors/");

AutoCargador::cargarConfiguracion([
  "espacios" => [
    "Controlador" => "../Controlador",
    "Modelo" => "../Modelo",
    "Libreria" => "../Libreria",
    "Nut" => "../Libreria/Nut"
  ]
]);

function consultaUsandoPOST($url, $parametros) {
    $consulta = \curl_init();
    \curl_setopt($consulta, CURLOPT_HTTPHEADER, "multipart/form-data");
    \curl_setopt($consulta, CURLOPT_URL, $url);
    \curl_setopt($consulta, CURLOPT_POST, true);
    \curl_setopt($consulta, CURLOPT_POSTFIELDS, http_build_query($parametros));
    \curl_setopt($consulta, CURLOPT_RETURNTRANSFER, true);
    $retorno = curl_exec($consulta);
    \curl_close($consulta);
    return $retorno;
}
