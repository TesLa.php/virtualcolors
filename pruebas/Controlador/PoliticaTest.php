<?php

namespace Controlador;

/**
 * Generated by PHPUnit_SkeletonGenerator on 2017-06-28 at 00:53:44.
 */
class PoliticaTest extends \PHPUnit\Framework\TestCase {

    /**
     * @var Politica
     */
    protected $objecto;

    protected function setUp() {
        parent::setUp();
        $this->objecto = new Politica();
    }

    public function proveedorGuardar() {
        return [
          ['empresa 1', '<p>descripcion empresa 1</p>', 'true'],
          ['empresa 2', '<p>empresa 2</p>', 'true'],
          ['empresa 1', 'actualizacion del primero', 'true'],
          [null, 'no insertado por constrain', 'false']
        ];
    }

    /**
     * @covers Controlador\Politica::guardar
     * @dataProvider proveedorGuardar
     */
    public function testGuardar($nombre, $descripcion, $respuesta) {
        $urlBase = URL_BASE;
        $imagen = realpath('../../publico/imagenes/add.png');

        $retorno = consultaUsandoPOST("$urlBase/politica/guardar", [
          'nombre-empresa' => $nombre,
          'descripcion-empresa' => $descripcion,
          'firma-empresa' => "@$imagen"
        ]);

        $this->assertEquals($respuesta, $retorno);
    }

}
